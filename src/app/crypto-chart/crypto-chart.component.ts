import { Component, OnInit, AfterViewInit, Input } from '@angular/core';
import { CryptocurrencyService } from '../cryptocurrency.service';
import { CryptoCurrency } from '../cryptocurrency';
import { Chart } from 'chart.js';
import { SelectionModel } from '@angular/cdk/collections';


@Component({
  selector: 'app-crypto-chart',
  templateUrl: './crypto-chart.component.html',
  styleUrls: ['./crypto-chart.component.css']
})
export class CryptoChartComponent implements OnInit {
  @Input() selection: SelectionModel<CryptoCurrency>;
  cryptocurrencies: CryptoCurrency[];
  invalue: number;
  outvalue: number;
  chart: any;
  chartType: string = "logarithmic";

  constructor(private cryptocurrencyService: CryptocurrencyService) { }

  ngOnInit() {
    this.getCryptocurrencies()
  }

  build() {
    const data = [], labels = [];
    this.cryptocurrencies.slice(0, 15).forEach(element => {
      labels.push(element.name)
      data.push(element.price_usd);
    });

    this.chart = new Chart('charts', {
      type: 'bar',
      data: {
        labels: labels,
        datasets: [{
          label: "in USD",
          data: data,
          backgroundColor: [
            'rgba(255, 99, 132, 0.2)',
            'rgba(54, 162, 235, 0.2)',
            'rgba(255, 206, 86, 0.2)',
            'rgba(75, 192, 192, 0.2)',
            'rgba(153, 102, 255, 0.2)',
            'rgba(255, 159, 64, 0.2)'
          ],
          borderColor: [
            'rgba(255,99,132,1)',
            'rgba(54, 162, 235, 1)',
            'rgba(255, 206, 86, 1)',
            'rgba(75, 192, 192, 1)',
            'rgba(153, 102, 255, 1)',
            'rgba(255, 159, 64, 1)'
          ],
          borderWidth: 1
        }
        ]
      },
      options: {
        responsive: true,
        scales: {
          xAxes: [{ ticks: { autoSkip: false } }],
          yAxes: [{
            type: 'logarithmic',
            ticks: {
              startFromZero: true,
            }
          }]
        }
      }
    });
  }

  changeChartType(value: string) {
    this.chart.options.scales.yAxes[0] = {
      display: true,
      type: value
    };
    this.chart.update();
  }

  updateChart() {
    const data = [], labels = [];
    this.cryptocurrencies.slice(0, 15).forEach(element => {
      labels.push(element.name)
      data.push(element.price_usd);
    });

    this.chart.data.labels = labels
    this.chart.data.datasets[0].data = data
    this.chart.update()
  }

  getCryptocurrencies(): void {
    this.cryptocurrencyService.getCryptoCurrencies()
      .subscribe((cryptocurrencies) => {
        if (this.selection) {
          this.cryptocurrencies = this.selection.selected;
        }
        else {
          this.cryptocurrencies = cryptocurrencies;
        }
        if (!this.chart) {
          this.build();
        }
        else {
          this.updateChart()
        }
      })
  }

  removeCurrency(currency: CryptoCurrency){
    this.selection.deselect(currency);
    this.cryptocurrencies = this.selection.selected;
    this.updateChart();
  }

}
