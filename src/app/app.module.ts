import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule }    from '@angular/common/http';
import { FormsModule }    from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialComponentsModule } from './/material-components.module';

import { AppComponent } from './app.component';
import { CryptocurrencyComponent} from './cryptocurrency/cryptocurrency.component';
import { CurrencyConverterComponent } from './currency-converter/currency-converter.component';
import { LoggerComponent } from './logger/logger.component';
import { CryptoChartComponent } from './crypto-chart/crypto-chart.component';

import { CryptocurrencyService } from './cryptocurrency.service';
import { LoggingService } from './logging.service';


@NgModule({
  declarations: [
    AppComponent,
    CryptocurrencyComponent,
    CurrencyConverterComponent,
    LoggerComponent,
    CryptoChartComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MaterialComponentsModule
  ],
  providers: [ CryptocurrencyService, LoggingService],
  bootstrap: [AppComponent]
})
export class AppModule { }
