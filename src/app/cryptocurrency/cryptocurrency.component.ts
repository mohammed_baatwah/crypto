import { Component, OnInit} from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { ViewChild } from '@angular/core/';
import { AfterViewInit } from '@angular/core/src/metadata/lifecycle_hooks';
import { DataSource } from '@angular/cdk/collections';
import { SelectionModel } from '@angular/cdk/collections';
import { CryptoCurrency } from '../cryptocurrency';
import { CryptocurrencyService } from '../cryptocurrency.service';

@Component({
  selector: 'app-cryptocurrency',
  templateUrl: './cryptocurrency.component.html',
  styleUrls: ['./cryptocurrency.component.css']
})
export class CryptocurrencyComponent implements OnInit, AfterViewInit {
  currency: CryptoCurrency = { id: "bitcoin", name: "Bitcoin", price_usd: 9000, symbol: "BTC" }
  currencies: CryptoCurrency[];
  displayedColumns = ['name', 'price_usd'];
  dataSource: MatTableDataSource<CryptoCurrency>;
  selection = new SelectionModel<CryptoCurrency>(true, [])
  selecting: boolean = false;
  comparing: boolean = false;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;


  constructor(private cryptocurrencyService: CryptocurrencyService) { }

  ngOnInit() {
    this.getCurrencies();
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  /**
   * Links this componenent to the API Service see {@link CryptocurrencyService}.
   */
  getCurrencies(): void {
    this.cryptocurrencyService.getCryptoCurrencies()
      .subscribe((currencies) => {
        this.currencies = currencies;
        if (!this.dataSource)
          this.dataSource = new MatTableDataSource<CryptoCurrency>([]);
        this.dataSource.data = currencies;
      })
  }

  coinSelected(element: CryptoCurrency) {
    // TODO: Show Dialog with all data.
  }

  toggleSelection() {
    this.selecting = !this.selecting;
  }

  toggleComparison() {
    this.comparing = !this.comparing;
  }

}