import { Component, OnInit } from '@angular/core';
import { CryptoCurrency } from '../cryptocurrency';
import { CryptocurrencyService } from '../cryptocurrency.service';
import { Chart } from 'chart.js';


@Component({
  selector: 'app-currency-converter',
  templateUrl: './currency-converter.component.html',
  styleUrls: ['./currency-converter.component.css']
})
export class CurrencyConverterComponent implements OnInit {
  selectedCurrency: CryptoCurrency;
  cryptocurrencies: CryptoCurrency[];
  invalue: number;
  outvalue: number;

  constructor(private cryptocurrencyService: CryptocurrencyService) { }

  ngOnInit() {
    this.getCryptocurrencies()
  }

  getCryptocurrencies(): void{
    this.cryptocurrencyService.getCryptoCurrencies()
      .subscribe((cryptocurrencies) => {
        this.cryptocurrencies = cryptocurrencies;
        this.selectedCurrency = this.cryptocurrencies && this.cryptocurrencies[0];
      })
  }

  getCryptocurrency(id: string): void{

  }

  get SelectedCurrency(){
    return this.selectedCurrency || undefined;
  }
  set SelectedCurrency(value){
    console.log("Hello");
    this.selectedCurrency = value as CryptoCurrency;
    this.convert(this.invalue);
  }

  print(cr){
    console.log(cr)
  }

  convert(changedValue){
    if(changedValue == this.invalue){ // USD -> CRYPTO
      this.outvalue = this.invalue / this.selectedCurrency.price_usd;
    }
    else{
      this.invalue = this.outvalue * this.selectedCurrency.price_usd;
    }
  }



}
