import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatPaginatorModule, MatTableModule } from '@angular/material'; // // Components for the currency list
import { MatFormFieldModule, MatSelectModule, MatOptionModule } from '@angular/material'; // Components for the currency converter 
import { MatCheckboxModule, MatCardModule, MatToolbarModule, MatMenuModule, MatProgressSpinnerModule } from '@angular/material';
import { MatButtonModule, MatInputModule } from '@angular/material';
import { MatButtonToggleModule, MatListModule , MatGridListModule} from '@angular/material';

@NgModule({
  imports: [
    CommonModule, MatPaginatorModule, MatTableModule, MatFormFieldModule, MatSelectModule, MatOptionModule, MatCheckboxModule, MatCardModule, MatToolbarModule, MatMenuModule, MatProgressSpinnerModule, MatButtonModule, MatInputModule, MatButtonToggleModule, MatListModule, MatGridListModule
  ],
  exports: [
    CommonModule, MatPaginatorModule, MatTableModule, MatFormFieldModule, MatSelectModule, MatOptionModule, MatCheckboxModule, MatCardModule, MatToolbarModule, MatMenuModule, MatProgressSpinnerModule, MatButtonModule, MatInputModule, MatButtonToggleModule, MatListModule, MatGridListModule
  ]
})
export class MaterialComponentsModule { }
