import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs/';
import { of } from 'rxjs/observable/of';
import { catchError, map, tap } from 'rxjs/operators';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { CryptoCurrency } from './cryptocurrency';
import { LoggingService } from './logging.service';




const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
}

@Injectable()
export class CryptocurrencyService {
  currencyapi: string = "https://api.coinmarketcap.com/v1/ticker/";
  cryptocurrencies: CryptoCurrency[];
  currencyUpdaterID: number;
  cryptoSub: BehaviorSubject<CryptoCurrency[]>;


  constructor(
    private http: HttpClient,
    private logger: LoggingService
  ) {
  }

  /** GET all currencies */
  getCryptoCurrencies(): Observable<CryptoCurrency[]> {
    if (!this.cryptoSub) {
      this.cryptoSub = new BehaviorSubject<CryptoCurrency[]>([]);
      setInterval(() => { this.updateCryptoCurrencies() }, 5 * 1000 * 60);
      this.updateCryptoCurrencies();
    }
    else {
      this.logger.add(`Fetched all Cryptocurrency from stored Data at ${new Date().toTimeString()}`)
    }

    return this.cryptoSub;
  }

  updateCryptoCurrencies(): void {
    this.http.get<CryptoCurrency[]>(this.currencyapi)
      .subscribe((currencies) => {
        //this.cryptocurrencies = currencies;
        this.cryptoSub.next(currencies);
        this.logger.add(`Updated all Cryptocurrency data at ${new Date().toTimeString()}`)
      }),
      catchError(this.handleError<CryptoCurrency[]>('Get All Currencies', []))
  }

  /**
   *  Get the data for a single cryptocrurrancy.
   * @param id a string representing the cryptocurrency
   */
  getCryptoCurrencyById(id: string) {
    return this.cryptoSub.value.find((el) => el.id === id);
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure

      // TODO: better job of transforming error for user consumption
      this.logger.add(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
